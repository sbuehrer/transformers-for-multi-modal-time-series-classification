# ClassFormer: Transformers for multivariate time series classification


## Implementation and Repository Structure
- **tutorial.ipynb** contains a step by step explination of the model an the training loop
- **src/main.py** contains the code for training on GPUs as predefined in the config files
- **src/ClassFormer.py** The heart of the code the Classsformer model and the training loop



## Contact-Info

Please feel free to contact me for any questions or cooperation opportunities. I'd be happy to help.
Simon Bührer
- Email: [sbuehrer@student.ethz.ch](sbuehrer@student.ethz.ch)
- LinkedIn: [www.linkedin.com/in/simon-buehrer](www.linkedin.com/in/simon-buehrer)

## Citation
```
@misc{buehrer2023classformer,
      title={ClassFormer: Transformers for Multivariate Time Series Classification}, 
      author={Simon Jonas Buehrer},
      year={2023},
      abstract ={The world of multivariate time-series classification is full of challenges, from figuring out human activities to spotting motion, dealing with ECG issues, and  classifying audio. Many methods are out there, using dense and convolution approaches. However, when it comes to really long time series, these methods struggle because their complexity grows rapidly, and spotting dependencies over long periods becomes impossible.\\ This thesis uses Transformers and adapts various techniques used for attention mechanism, resulting in a high-performing network evaluated on 22 different datasets. Key innovations include using continuous wavelet transform for frequency enhancement, data patching to reduce model complexity, and a hierarchical three-stage attention mechanism capturing cross-dependencies across time, dimensions, and frequencies at different scales. Furthermore, data-driven masking was tested, which improves performance during training and makes pre-training unnecessary.\\}
}
```
## To-do
- [ ] Parameter tuning maybe using [neural architecture search (NAS)](https://docs.gitlab.com/ee/user/project/members/)
- [ ] Use different wavelets and detect if data is already in spectral form
- [ ] Comment classformer.py and main.py, copy text from tutorial.ipynb
