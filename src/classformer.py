# Libaries for math operations and plotting
import numpy as np
from scipy import signal

# Libaries for machine learning tools
import tensorflow as tf

import time

def pad_dataset(x,maxlen):
    assert isinstance(x,list)
    assert maxlen>0
    y=[]
    for observation in x:
        temp=[]
        for series in observation:
            temp.append(np.pad(series, (0,maxlen-len(series)), 'symmetric'))
        y.append(temp)
    return np.array(y)

# Wavelet Transformation
class WaveletTransform():
  def __init__(self, freq_size=64):
    self.freq_size = freq_size
    self.norm=tf.keras.layers.LayerNormalization(axis=1)

  def call(self, x):
    output_list = []
    for ind in range(tf.shape(x)[0]):
      dim_list=[]
      for dim in range(tf.shape(x)[1]):
        dur = x[ind][dim].shape[0]
        freq = np.linspace(1, dur/2, self.freq_size)
        widths = 10*dur / (2*freq*np.pi)
        cwtm = np.absolute(signal.cwt(x[ind][dim], signal.morlet2, widths, w=10))
        dim_list.append(self.norm(tf.transpose(cwtm,perm=[1,0])))
      output_list.append(tf.stack(dim_list))
    scalograms = tf.stack(output_list)
    return scalograms

# Dimension PatchWise Embedding
def ceil_to_b(a,b):
    return -1 * (-a // pow(2,b))

class DimensionPatchWiseEmbedding(tf.keras.layers.Layer):
  def __init__(self, size_T=10,size_F=4,d_model=20,levels=3):
    super().__init__()
    self.size_T = size_T
    self.size_F = size_F
    self.levels=levels
    self.d_model = d_model

  def build(self, input_shape):
    # learnable projection weights
    self.w = self.add_weight("projection_weights",shape=(self.d_model,self.size_T*self.size_F), initializer="random_normal", trainable=True)
    # learnable position weights
    b_shape = (input_shape[1],)+(int(np.ceil(input_shape[2]/self.size_T)),int(np.ceil(input_shape[3]/self.size_F)),self.d_model)
    self.b = self.add_weight("positional_weights",shape=b_shape, initializer="random_normal", trainable=True)

  def call(self, x):
    patched_x  = []
    for obs in tf.split(x, num_or_size_splits=int(x.shape[1]), axis=1):
      obs=tf.transpose(obs,[0,2,3,1])
      # extract the patches from the scalogram
      patches = tf.image.extract_patches(images=obs,
                                        sizes=[1, self.size_T, self.size_F, 1],
                                        strides=[1, self.size_T, self.size_F, 1],
                                        rates=[1, 1, 1, 1],
                                        padding='SAME') # Notice "SAME" means the patchs will be padded with zeros. Use "VALID" for no padding.
      patched_x.append(patches)
      # Stack up all observations in a batch
    patched_x=tf.stack(patched_x, axis=1)
    # Make the linear projection and add the positional embedding
    patched_x=tf.expand_dims(patched_x, 5)
    # Project an add positional embedding
    patched_x= tf.squeeze(tf.matmul(self.w,patched_x))+self.b
    # Pad to multiple of 2
    new_f=int(ceil_to_b(tf.shape(patched_x)[3],self.levels)*pow(2,self.levels)-tf.shape(patched_x)[3])
    new_t=int(ceil_to_b(tf.shape(patched_x)[2],self.levels)*pow(2,self.levels)-tf.shape(patched_x)[2])
    paddings = tf.convert_to_tensor([[0, 0], [0, 0],[0, new_t],[0, new_f],[0, 0]])
    if(len(tf.shape(patched_x))<5):
      patched_x=tf.expand_dims(patched_x, 0)
    return tf.pad(patched_x, paddings, "CONSTANT"), tf.pad(tf.ones(tf.shape(patched_x)), paddings, "CONSTANT")

# Transformer Encoder
class TransformerEncoder(tf.keras.layers.Layer):
  def __init__(self,num_heads=4, dropout_rate=0.1,cross_direction="time"):
    super().__init__()
    self.num_heads=num_heads
    self.dropout_rate=dropout_rate
    self.cross_dir=cross_direction
    self.dir={"dimension":1,"time":2,"frequency":3}[cross_direction] # 1 Cross-Dimension, 2 Cross-Time, 3 Cross-Frequency

  def build(self, input_shape):
    self.mha = tf.keras.layers.MultiHeadAttention(num_heads=self.num_heads,key_dim=int(np.ceil(input_shape[-1]/self.num_heads)),attention_axes=(self.dir),kernel_initializer="random_normal")
    self.layernorm = tf.keras.layers.LayerNormalization()
    self.dense=tf.keras.layers.Dense(input_shape[-1],activation='relu')
    self.add = tf.keras.layers.Add()
    self.dropout = tf.keras.layers.Dropout(self.dropout_rate)


  def call(self, x,mask,reconstructing,training):
    if(reconstructing == False):
       mask=tf.experimental.numpy.moveaxis(mask, self.dir, -2)
       mask=tf.reduce_max(mask, -1)
       self.mask=mask[:,:,:, tf.newaxis, tf.newaxis, :]
       y=x
    else:
      y=tf.cast(tf.expand_dims(self.series_mask,axis=-1),tf.float64) * tf.cast(x,tf.float64)
    attn_output, attn_scores = self.mha(
          query=x,
          key=x,
          value=x,
          attention_mask=self.mask,
          return_attention_scores=True)
      # Cache the attention scores for plotting later.
    self.last_attn_scores=attn_scores
    x = self.add([y, attn_output])
    x = self.layernorm(x)
    x = self.dropout(x,training)
    x = self.add([x, self.dense(x)])
    x = self.layernorm(x)
    return x

  def get_tss_mask(self,mask_ratio=0.2,top_ratio=0.1):
      mask = self.last_attn_scores
      top_quantil=np.quantile(mask,1-top_ratio)
      condlist = [mask>=top_quantil, mask>0]
      choicelist = [2,1]
      mask = np.select(condlist, choicelist, 0)
      unique, counts = np.unique(mask, return_counts=True)
      count=dict(zip(unique, counts))
      mask[mask==2]=np.random.randint(1,100,count[2])
      mask[mask>=100*(1-mask_ratio)]=0
      mask[mask>0]=1
      self.mask=mask

      self.series_mask=np.array(tf.reduce_sum(tf.reduce_sum(mask, -1),-2))
      lowest=np.quantile(self.series_mask,top_ratio)
      self.series_mask[self.series_mask<lowest]=0
      self.series_mask[self.series_mask>0]=1
      self.series_mask = tf.experimental.numpy.moveaxis(self.series_mask, -1, self.dir)
      return mask
  
  def get_attention_score(self):
    return self.last_attn_scores

# Three Stage Attention
class ThreeStageAttention(tf.keras.layers.Layer):
  def __init__(self,num_heads,dropout_rate,mask_ratio=0.2,top_ratio=0.1):
    super().__init__()
    self.dim_ca = TransformerEncoder(num_heads,dropout_rate,cross_direction="dimension")
    self.time_ca = TransformerEncoder(num_heads,dropout_rate,cross_direction="time")
    self.freq_ca = TransformerEncoder(num_heads,dropout_rate,cross_direction="frequency")
    self.mask_ratio=mask_ratio
    self.top_ratio=top_ratio

  def call(self,x,mask,reconstructing,training):
    if(reconstructing):
      self.time_ca.get_tss_mask(self.mask_ratio,self.top_ratio)
      self.freq_ca.get_tss_mask(self.mask_ratio,self.top_ratio)
      self.dim_ca.get_tss_mask(self.mask_ratio,self.top_ratio)

    x=self.time_ca(x,mask,reconstructing,training)
    x=self.freq_ca(x,mask,reconstructing,training)
    x=self.dim_ca(x,mask,reconstructing,training)
    return x

  def get_attention_score(self):
    return (self.dim_ca.get_attention_score(),self.time_ca.get_attention_score(),self.freq_ca.get_attention_score())

# Down Scale
class DownScale(tf.keras.layers.Layer):
  def __init__(self,d_model):
    super().__init__()
    self.d_model=d_model
    self.w = self.add_weight("downscale_projection",shape=(self.d_model*4,self.d_model), initializer="random_normal", trainable=True)


  def call(self, x,mask):
    concat_shape=[tf.shape(x)[0],tf.shape(x)[1],tf.shape(x)[2]//2,tf.shape(x)[3]//2,tf.shape(x)[4]*4]
    mask_shape=[tf.shape(x)[0],tf.shape(x)[1],tf.shape(x)[2]//2,tf.shape(x)[3]//2,tf.shape(x)[4],4]
    x=tf.reshape(x,concat_shape)
    mask=tf.reshape(mask,mask_shape)
    mask=tf.reduce_max(mask, -1)
    return tf.matmul(x,self.w),mask

# Fully Connected for Classification
class FullyConnectedClassification(tf.keras.layers.Layer):
  def __init__(self, output_size=5, dff=256, dropout_rate=0.1,num_tsa=3,dim=3):
    super().__init__()
    self.denseReLu1=[]
    self.dim=dim
    for i in range(num_tsa):
      self.denseReLu1.append(tf.keras.layers.Dense(20, activation='relu'))
    self.denseReLu2 = tf.keras.layers.Dense(dff, activation='relu')
    self.denseReLu3 = tf.keras.layers.Dense(dff, activation='relu')
    self.dropout =  tf.keras.layers.Dropout(dropout_rate)
    self.denseSig =  tf.keras.layers.Dense(output_size,activation='sigmoid')
    self.softmax = tf.keras.layers.Softmax()
    self.flatten=tf.keras.layers.Flatten()


  def call(self, x,training=False):
    result=[]
    for ind,z in enumerate(x):
        z=tf.transpose(z, perm=[0,1,3,4,2])
        #z=tf.reshape(z,[tf.shape(z)[0],tf.shape(z)[1],tf.shape(z)[2],tf.shape(z)[3]*tf.shape(z)[4]])
        result.append(self.flatten(self.denseReLu1[ind](z)))
    x = tf.concat(result,axis=1)
    x = self.dropout(x,training)
    x = self.denseReLu2(x)
    x = self.denseReLu3(x)
    x = self.denseSig(x)
    return self.softmax(x)

# Fully Connected for Reconstruction
class FullyConnectedReconstruction(tf.keras.layers.Layer):
  def __init__(self, output_size=5, recon_dff=1024, dropout_rate=0.1,num_tsa=3,dim=3):
    super().__init__()
    self.denseReLu1=[]
    self.dim=dim
    for i in range(num_tsa):
      self.denseReLu1.append(tf.keras.layers.Dense(20, activation='relu'))
    self.dropout =  tf.keras.layers.Dropout(dropout_rate)
    self.denseReLu2 =  tf.keras.layers.Dense(recon_dff,activation='relu')
    self.denseReLu3 =  tf.keras.layers.Dense(recon_dff,activation='relu')
    self.denseReLu4 =  tf.keras.layers.Dense(output_size)
    self.flatten=tf.keras.layers.Flatten()

  def call(self, x,shape,training=False):
    result=[]
    for ind,z in enumerate(x):
        z=tf.transpose(z, perm=[0,1,3,4,2])
        #z=tf.reshape(z,[tf.shape(z)[0],tf.shape(z)[1],tf.shape(z)[2],tf.shape(z)[3]*tf.shape(z)[4]])
        result.append(self.flatten(self.denseReLu1[ind](z)))
    x = tf.concat(result,axis=1)
    x = self.dropout(x,training)
    x = self.denseReLu2(x)
    x = self.denseReLu3(x)
    x = self.denseReLu4(x)
    x = tf.reshape(x,[x.shape[0]]+shape)
    return x




class Classformer(tf.keras.Model):
  def __init__(self,output_size,recon_size,d_model,num_heads,dropout_rate,dff,recon_dff,num_tsa,size_T,size_F,mask_ratio,top_ratio):
    super().__init__()
    self.batchnorm = tf.keras.layers.BatchNormalization()
    self.patching = DimensionPatchWiseEmbedding(size_T=size_T,size_F=size_F,d_model=d_model,levels=num_tsa)
    self.num_tsa=num_tsa
    self.tsa=[]
    self.downscale=[]
    for encod in range(self.num_tsa-1):
      self.downscale.append(DownScale(d_model))
      self.tsa.append(ThreeStageAttention(num_heads,dropout_rate,mask_ratio,top_ratio))
    self.tsa.append(ThreeStageAttention(num_heads,dropout_rate,mask_ratio,top_ratio))
    self.fulldenseclass=FullyConnectedClassification(output_size=output_size, dff=dff, dropout_rate=dropout_rate,num_tsa=num_tsa,dim=recon_size[0])
    self.fulldensereconst=FullyConnectedReconstruction(output_size=np.prod(recon_size), recon_dff=recon_dff, dropout_rate=dropout_rate,num_tsa=num_tsa,dim=recon_size[0])
    self.recon_size=recon_size

  def call(self, inputs,reconstructing=False, training=False):
    self.intermediate_val=[]
    # Normalize the Scalograms
    x=self.batchnorm(inputs)

    # Dimension Patch Wise Embedding
    z,mask = self.patching(x)
    self.intermediate_val.append(z)
    self.original_mask=mask

    # Hierarchical TSA Stack
    result=[]
    for level in range(self.num_tsa-1):
      z=self.tsa[level](z,mask,reconstructing,training)
      result.append(z)
      self.intermediate_val.append(z)
      z,mask=self.downscale[level](z,mask)
    z=self.tsa[self.num_tsa-1](z,mask,reconstructing,training)
    self.intermediate_val.append(z)
    result.append(z)

    #Concat the results from the stack
    self.intermediate_val.append(result)

    #Fully Connected Layer which map the result to reconstruction or classification
    if(reconstructing):
      self.fulldenseclass.trainable = False
      self.fulldensereconst.trainable = True
      result=self.fulldensereconst(result,self.recon_size, training)
    else:
      self.fulldensereconst.trainable = False
      self.fulldenseclass.trainable = True
      result=self.fulldenseclass(result, training)
    return result

  def get_intermediate_val(self):
    return self.intermediate_val
  
  def get_attention_score(self):
    scores=[]
    for level in range(self.num_tsa):
      scores.append(self.tsa[level].get_attention_score())
    return scores


class CustomSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
  def __init__(self, d_model, warmup_steps=5,steepness=1):
    super().__init__()
    self.d_model = d_model
    self.d_model = tf.cast(self.d_model, tf.float32)
    self.warmup_steps = warmup_steps
    self.steepness=steepness

  def __call__(self, step):
    step = tf.cast(step, dtype=tf.float32)
    arg1 = tf.math.rsqrt(step)*((0.8**(step-self.warmup_steps))**self.steepness)
    arg2 = step * (self.warmup_steps ** -1.5)

    return tf.math.rsqrt(self.d_model) * tf.math.minimum(arg1, arg2)*1e-02


# loss function
def loss(model, x, y, training,reconstructing):
  class_loss_object = tf.keras.losses.CategoricalCrossentropy()
  reco_loss_object=tf.keras.losses.MeanSquaredError()
  y_ = model(x, reconstructing=reconstructing,training=training)
  if(reconstructing):
    return reco_loss_object(y_true=y, y_pred=y_)
  else:
    return class_loss_object(y_true=y, y_pred=y_)

def grad(model, inputs, targets, reconstructing):
  with tf.GradientTape() as tape:
    loss_value = loss(model, inputs, targets, reconstructing=reconstructing, training=True)
  return loss_value, tape.gradient(loss_value, model.trainable_variables)

def pretty_time_delta(seconds):
    seconds = int(seconds)
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        return '%dd%dh%dm%ds' % (days, hours, minutes, seconds)
    elif hours > 0:
        return '%dh%dm%ds' % (hours, minutes, seconds)
    elif minutes > 0:
        return '%dm%ds' % (minutes, seconds)
    else:
        return '%ds' % (seconds,)

def pretty_metrics(values,names):
    out=""
    for i,val in enumerate(values):
        pre=""
        if(i//len(names)==1):
            pre="val_"
        out+=" - "+pre+names[i%len(names)]+": %.3f" % val
    return str(out)

def test_inference(model,class_metrics,test_dataset):
  for (x, y) in test_dataset:
    y_pred = model(x,reconstructing=False, training=False)
    for metric in class_metrics["test"].values():
      metric.update_state(y,y_pred)

  temp_metric=[]
  for metric in class_metrics["test"].values():
    temp_metric.append(float(metric.result()))
  return temp_metric

# Training
def training(test_number,
            sample_index,
            dataset_name,
            logger,
            reconstructing_mode,
            model,
            class_optimizer,
            reco_optimizer,
            train_dataset,
            batch_size,
            test_dataset,
            class_metrics,
            reco_metrics,
            num_epochs,
            patience,
            save_mode,
            num_folds):
  
  history={"train":[],"val":[],"test":[]}
  max_val_accuracy=0
  early_stopping_count=0

  # shuffle the train data
  indices = tf.range(start=0, limit=tf.shape(train_dataset[0])[0], dtype=tf.int32)
  idx = tf.random.shuffle(indices)
  x_full,y_full,cwtm_full = (tf.gather(train_dataset[0], idx),tf.gather(train_dataset[1], idx),tf.gather(train_dataset[2], idx))
  test_dataset = tf.data.Dataset.from_tensor_slices(test_dataset)
  test_dataset = test_dataset.batch(batch_size)

  for epoch in range(num_epochs):
    start = time.time()

    # split the dataset in val and train
    if(num_folds>1):
      fold=epoch%num_folds
      shift=tf.cast(tf.shape(x_full)[0]/num_folds,dtype=tf.int64)
      x_roll,y_roll,cwtm_roll = tf.roll(x_full,shift=shift*fold,axis=0),tf.roll(y_full,shift=shift*fold,axis=0),tf.roll(cwtm_full,shift=shift*fold,axis=0)
      x_val,x_train=(x_roll[-shift:],x_roll[:-shift])
      y_val,y_train=(y_roll[-shift:],y_roll[:-shift])
      cwtm_val,cwtm_train=(cwtm_roll[-shift:],cwtm_roll[:-shift])

      val_dataset = tf.data.Dataset.from_tensor_slices((x_val,y_val,cwtm_val))
      train_dataset = tf.data.Dataset.from_tensor_slices((x_train,y_train,cwtm_train))
      train_dataset = train_dataset.batch(batch_size)
      val_dataset = val_dataset.batch(batch_size)
    else:
      train_dataset = tf.data.Dataset.from_tensor_slices((x_full,y_full,cwtm_full)).batch(batch_size)
      val_dataset=None



    # Training loop - using batches of ...
    for (x, y, cwtm) in train_dataset:
      # Optimize the model with Classification
      loss_value, grads = grad(model, x, y,reconstructing=False)
      class_optimizer.apply_gradients(zip(grads, model.trainable_variables))
      y_pred=model(x,reconstructing=False, training=True)
      for metric in class_metrics["train"].values():
        metric.update_state(y,y_pred)

      if(reconstructing_mode):
        # Optimize the model with Reconstruction
        loss_value, grads = grad(model, x, cwtm,reconstructing=True)
        reco_optimizer.apply_gradients(zip(grads, model.trainable_variables))
        y_pred=model(x,reconstructing=True, training=True)
        for metric in reco_metrics["train"].values():
          metric.update_state(cwtm,y_pred)


    # Validation
    if(val_dataset != None):
      for (x, y,cwtm) in val_dataset:
        y_pred = model(x,reconstructing=False, training=False)
        for metric in class_metrics["val"].values():
          metric.update_state(y,y_pred)

        if(reconstructing_mode):
          y_pred = model(x,reconstructing=True, training=False)
          for metric in reco_metrics["val"].values():
            metric.update_state(cwtm,y_pred)

    # End epoch
    temp_metric=[]
    for metric in class_metrics["train"].values():
        temp_metric.append(float(metric.result()))
    for metric in reco_metrics["train"].values():
        temp_metric.append(float(metric.result()))
    history["train"].append(temp_metric)

    if(val_dataset != None):
      temp_metric=[]
      for metric in class_metrics["val"].values():
          temp_metric.append(float(metric.result()))
      for metric in reco_metrics["val"].values():
          temp_metric.append(float(metric.result()))
      history["val"].append(temp_metric)
      if(max_val_accuracy>=temp_metric[0]):
        early_stopping_count+=1
      else:
        max_val_accuracy=temp_metric[0]
        early_stopping_count=0
      time_elapsed = (time.time() - start)
      logger.info("Epoch {:03d} - {} - train accuracy: {:.3f} - train mse: {:.3f} - val accuracy: {:.3f} - val mse: {:.3f} -> {}".format(epoch,
                                                                                                                                  str(pretty_time_delta(time_elapsed)),
                                                                                                                                  history["train"][-1][0],
                                                                                                                                  history["train"][-1][-1],
                                                                                                                                  history["val"][-1][0],
                                                                                                                                  history["val"][-1][-1],
                                                                                                                                  dataset_name))
    else:
      time_elapsed = (time.time() - start)
      logger.info("Epoch {:03d} - {} - train accuracy: {:.3f} - train mse: {:.3f} -> {}".format(epoch,
                                                                                                str(pretty_time_delta(time_elapsed)),
                                                                                                history["train"][-1][0],
                                                                                                history["train"][-1][-1],
                                                                                                dataset_name))

    if(early_stopping_count>=patience):
      if(save_mode>0):
        logger.info("Saving {} model under ./checkpoints/{}/{}/{}/{}.mask.ckpt".format(dataset_name,test_number,dataset_name,sample_index,num_epochs))
        model.save_weights('./checkpoints/{}/{}/{}/{}.mask.ckpt'.format(test_number,dataset_name,sample_index,num_epochs))
      history["test"].append(test_inference(model,class_metrics,test_dataset))
      return history
  if(save_mode>0):
    logger.info("Saving {} model under ./checkpoints/{}/{}/{}/{}.mask.ckpt".format(dataset_name,test_number,dataset_name,sample_index,num_epochs))
    model.save_weights('./checkpoints/{}/{}/{}/{}.mask.ckpt'.format(test_number,dataset_name,sample_index,num_epochs))
  history["test"].append(test_inference(model,class_metrics,test_dataset))
  return history
