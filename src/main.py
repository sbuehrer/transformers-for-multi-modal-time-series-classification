# load the model and training functions
import classformer as cf

# load the libaries
from aeon.datasets import load_classification
import logging
import json 
import os
import time
import numpy as np
import tensorflow as tf 
import random
import pandas as pd
import multiprocessing

# set cuda location
os.environ['XLA_FLAGS'] = '--xla_gpu_cuda_data_dir=/usr/lib/cuda'

 # Code for not using CPU
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


# create a Logger
path="/scratch/sbuehrer/ClassFormer" # path to ClassFormer

os.chdir(path)
test_num="06"
test_config=[i for i in os.listdir("configs/test_configs") if test_num in i][0]
print(test_config)
with open("configs/test_configs/{}".format(test_config), "r") as read_file:
    test_config = json.load(read_file)

log_filename = "logs/{}_{}.log".format(test_config["test_identifier"],test_config["test_describtion"])
os.makedirs(os.path.dirname(log_filename), exist_ok=True)
file_handler = logging.FileHandler(log_filename, mode="w", encoding=None, delay=False)
logging.basicConfig(filename=log_filename,format="%(asctime)s: %(levelname)s: %(message)s",filemode='w',level=logging.DEBUG)


def task(path,epochs):
    global idx
    logging.info("Working on GPU {}".format(idx))
    with tf.device('/gpu:{}'.format(idx)):
        with open("configs/dataset_configs/{}.json".format(path), "r") as read_file:
            config = json.load(read_file)

        # Import a dataset
        try:
            X_train, y_train, meta_data = load_classification(config["dataset_name"],split="train",return_metadata=True)
            X_test, y_test = load_classification(config["dataset_name"],split="test",return_metadata=False)
            # Handle dataset with unequal length
            if(not meta_data["equallength"]):
                maxlen=max(max([max(map(len, observation)) for observation in X_train]),max([max(map(len, observation)) for observation in X_test]))
                X_train=cf.pad_dataset(X_train,maxlen)
                X_test=cf.pad_dataset(X_test,maxlen)
            logging.info("Succesfully imported {} on GPU {}".format(config["dataset_name"],idx))
        except Exception as e:
            logging.error("Unable to import the datasets {}- {}".format(config["dataset_name"],str(e)))

        try:
            integer_mapping = {x: i for i,x in enumerate(meta_data["class_values"])}
            vec_train = [integer_mapping[word] for word in y_train]
            vec_test = [integer_mapping[word] for word in y_test]
            output_encoder = tf.keras.layers.CategoryEncoding(num_tokens=len(meta_data["class_values"]), output_mode="one_hot")
            y_train=output_encoder(vec_train)
            y_test=output_encoder(vec_test)
        except Exception as e:
            logging.error("Unable to One-Hot encode the outputs - {}".format(str(e)))

            
        start =time.time()
        try:
            tf.keras.backend.clear_session()
            wavelet_transform = cf.WaveletTransform(freq_size=config["freq_domain_size"])
            X_train=wavelet_transform.call(X_train)
            X_test=wavelet_transform.call(X_test)

            scal=[]
            lenT=tf.cast(np.ceil(tf.shape(X_train)[2]/config["recon_cwtm"]),dtype=tf.int64)
            pool_2d = tf.keras.layers.AveragePooling2D(pool_size=(lenT, 4), strides=(lenT, 4), padding='valid')
            for x_temp in tf.unstack(X_train,axis=1):
                scal.append(tf.squeeze(pool_2d(tf.expand_dims(x_temp,axis=-1))))
            scal=tf.stack(scal,axis=1)

            train_dataset = (X_train,y_train,scal)
            test_dataset = (X_test,y_test)


            logging.info("Duration of wavelet transform: {:.1f} seconds".format(time.time() - start))
        except Exception as e:
            logging.error("Wavelet Transform Error - {}".format(str(e)))

        # Grid search for hyperparameter
        max_samples=len(config["batch_size"])*len(config["d_model"])*len(config["num_heads"])*len(config["num_tsa"])*len(config["num_tsa"])*len(config["size_T"])*len(config["size_F"])*len(config["top_ratio"])*len(config["mask_ratio"])
        for sample_index in range(min(config["num_samples"],max_samples)):
            batch_size = random.choice(config["batch_size"])
            d_model = random.choice(config["d_model"])
            num_heads = random.choice(config["num_heads"])
            num_tsa = random.choice(config["num_tsa"])
            size_T = random.choice(config["size_T"])
            size_F = random.choice(config["size_F"])
            top_ratio = random.choice(config["top_ratio"])
            mask_ratio = random.choice(config["mask_ratio"])

            metrics={"val":[],"test":[],"train":[]}
            for iteration in range(test_config["repetition"]):
                logging.info("#{} iter: {} - {}".format(sample_index,iteration,(batch_size,d_model,num_heads,num_tsa,size_T,size_F,top_ratio,mask_ratio)))
                try:
                    tf.keras.backend.clear_session()
                    model=cf.Classformer(
                        output_size=len(meta_data["class_values"]),
                        recon_size=scal.shape[1:],
                        d_model=d_model,
                        num_heads=num_heads,
                        dropout_rate=config["dropout_rate"],
                        dff=config["dff"],
                        recon_dff=config["recon_dff"],
                        num_tsa=num_tsa,
                        size_T=size_T,
                        size_F=size_F,
                        mask_ratio=mask_ratio,
                        top_ratio=top_ratio)
                except Exception as e:
                    logging.error("Unable to init model - {}".format(str(e)))

                try:
                    if(test_config["warmup"]):
                        recon_rate = cf.CustomSchedule(d_model,10,0.1)
                        class_rate = cf.CustomSchedule(d_model,10,0)
                        class_optimizer = tf.keras.optimizers.Adam(class_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)
                        reco_optimizer = tf.keras.optimizers.Adam(recon_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)
                    else:
                        class_optimizer = tf.keras.optimizers.Adam(learning_rate=0.0002, beta_1=0.9, beta_2=0.98, epsilon=1e-9)
                        reco_optimizer = tf.keras.optimizers.Adam(learning_rate=0.0002, beta_1=0.9, beta_2=0.98, epsilon=1e-9)
                except Exception as e:
                    logging.error("Unable to init optimizers - {}".format(str(e)))

                try:
                    class_metrics = {"train":
                                        {"accuracy":tf.keras.metrics.CategoricalAccuracy(),
                                    "auc":tf.keras.metrics.AUC()},
                                    "val":
                                        {"accuracy":tf.keras.metrics.CategoricalAccuracy(),
                                    "auc":tf.keras.metrics.AUC()},
                                    "test":
                                        {"accuracy":tf.keras.metrics.CategoricalAccuracy(),
                                    "auc":tf.keras.metrics.AUC()}
                                    }
                    reco_metrics = {"train":
                                    {"mse": tf.keras.metrics.MeanSquaredError()},
                                    "val":
                                    {"mse": tf.keras.metrics.MeanSquaredError()},
                                    "test":
                                    {"mse": tf.keras.metrics.MeanSquaredError()} 
                                    }

                    history = cf.training(test_number=test_config["test_identifier"],
                                        sample_index="{}_{}".format(sample_index,iteration),
                                        dataset_name=config["dataset_name"],
                                        logger=logging.getLogger(),
                                        reconstructing_mode=test_config["reconstructing_mode"],
                                        model=model,
                                        class_optimizer=class_optimizer,
                                        reco_optimizer=reco_optimizer,
                                        train_dataset=train_dataset,
                                        test_dataset=test_dataset,
                                        batch_size=batch_size,
                                        class_metrics=class_metrics,
                                        reco_metrics=reco_metrics,
                                        num_epochs=epochs,
                                        patience=config["patience"],
                                        save_mode=test_config["save_mode"],
                                        num_folds=test_config["num_folds"])
                    logging.info("Finished Training - {}".format(config["dataset_name"]))
                except Exception as e:
                    logging.error("Unable to train '{}'- {}".format(config["dataset_name"],str(e)))

                try:
                    meta={"dataset_name": config["dataset_name"],
                            "freq_domain_size": config["freq_domain_size"],
                            "reconstructing_mode":test_config["reconstructing_mode"],
                            "d_model" : d_model,
                            "num_heads" : num_heads,
                            "dropout_rate":config["dropout_rate"],
                            "dff":config["dff"],
                            "recon_dff":config["recon_dff"],
                            "num_tsa":num_tsa,
                            "size_T":size_T,
                            "size_F":size_F,
                            "mask_ratio":mask_ratio,
                            "top_ratio":top_ratio,
                            "batch_size":batch_size,
                            "epochs":epochs,
                            "patience":config["patience"],
                            "cwtm_shape":str(scal.shape[1:])}

                    if(test_config["num_folds"]>1):
                        metrics["val"].append(history["val"][-1][0])
                        meta["val accuracy"]=history["val"][-1][0]
                    else:
                        metrics["train"].append(history["train"][-1][0])

                    os.makedirs(os.path.dirname('./checkpoints/{}/{}/{}_{}/'.format(test_config["test_identifier"],config["dataset_name"],sample_index,iteration)), exist_ok=True)
                    with open('./checkpoints/{}/{}/{}_{}/raw_data.json'.format(test_config["test_identifier"],config["dataset_name"],sample_index,iteration), 'w') as f:
                        json.dump({**meta, **history}, f)
                    metrics["test"].append(history["test"][0][0])
                    logging.info("Intermediate Result {} - {} - {} - Test Accuracy: {}".format(sample_index,iteration,config["dataset_name"],history["test"][0][0]))
                except Exception as e:
                    logging.error("Unable to export model - {}".format(str(e)))

            try:
                if(test_config["num_folds"]>1):
                    std,mean = np.std(np.array(metrics["val"])),np.mean(np.array(metrics["val"]))
                else:
                    std,mean = np.std(np.array(metrics["train"])),np.mean(np.array(metrics["train"]))
                T_std,T_mean = np.std(np.array(metrics["test"])),np.mean(np.array(metrics["test"]))
                result_df["index"].append(sample_index)
                result_df["dataset"].append(config["dataset_name"])
                result_df["acc"].append(mean)
                result_df["std"].append(std)
                result_df["T_acc"].append(T_mean)
                result_df["T_std"].append(T_std)
                logging.info("Final Result {} - {} : {} std {}".format(sample_index,config["dataset_name"],mean,std))
            except Exception as e:
                logging.error("Unable to update acc results - {}".format(str(e)))

        try:
            del X_train
            del X_test
            df = pd.DataFrame(data=result_df)
            logging.info("Created DataFrame - {}".format(config["dataset_name"]))
        except Exception as e:
            logging.error("Unable to delete dataset - {}".format(str(e)))
        return df

def init(queue):
    global idx
    idx = queue.get()
ids = [0, 1, 2, 3]
manager = multiprocessing.Manager()
idQueue = manager.Queue()
for i in ids:
    idQueue.put(i)


if __name__ == "__main__":
    # turn on memory growth
    os.environ['TF_FORCE_GPU_ALLOW_GROWTH '] = 'true'

    result_df={"index":[],"dataset":[],"acc":[],"std":[],"T_acc":[],"T_std":[]}
    pool = multiprocessing.Pool(4, init, (idQueue,))
    results = pool.starmap(task, test_config["config_filenames"].items())
    combined_df = pd.concat(results, ignore_index=True)
    combined_df.to_csv('results/{}_{}.csv'.format(test_config["test_identifier"],test_config["test_describtion"]))
    pool.close()
    pool.join()
